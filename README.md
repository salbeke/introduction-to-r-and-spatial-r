# Introduction to R and Spatial R

This 1-day workshop provides an introduction to using R and the spatial packages one can use for performing spatial analyses.  There are two parts to this workshop:

1.  Installing the software needed to actively participate in the demonstrations
2.  A live demonstration in which we will cover some of the basic R functions and syntax for both Base R and Tidyverse. Then we will move onto conducting spatial analyses. In this case we will learn to fit a simple species distribution model, using R Spatial to generate the data.frame for fitting the model. 

## Install Software
Before you begin, there are several pieces of software that need to be installed if you want to use R. I list the installation instructions here: [Workshop Software Installation](https://gitlab.com/salbeke/introduction-to-r-and-spatial-r/-/wikis/Installing-Program-R-and-other-Software). 

## Create your GitLab Account
I think it is important for you to learn about Code Repositories. I prefer to use GitLab over GitHub, but you can choose whichever platform you prefer. If you choose GitLab because you want to follow me explicitly during the workshop, go here and create an account prior to the course: [GitLab](https://gitlab.com/)  We will create a new repository together and go through all of the steps so that you can back up your code to the cloud as well as collaboratively work on code and software applications!


## Prerequisite: 
It will be ideal if you can have the following data downloaded to your personal machine prior to the course. There are two zip files to have handy, and during the course I will have you follow along as to what to do with the zip files, so just have them in a location where you know where they are. Finally, installing R Packages that extend its capabilities is crucial. 
1. Download the Tutorial Data: [R_Intro.zip](https://pathfinder.arcc.uwyo.edu/wygisc/Albeke_Courses/IntroR_Spatial/R_Intro.zip)
2. Download the Spatial Data: [R_SpatialExampleData.zip](https://pathfinder.arcc.uwyo.edu/wygisc/Albeke_Courses/IntroR_Spatial/R_SpatialExampleData.zip)
3. Open R Studio, in the bottom-right pane you will see a set of tabs, click on Packages. You will then see a button 'Install'. Use that to install the following packages that we will need for this workshop:
    -  **tidyverse, readxl, lubridate, plotly, clipr, curl, data.table, DBI, dbplyr, devtools, doParallel, FedData, foreach, future, future.apply, googlesheets4, gridExtra, here, htmltools, htmlwidgets, httr, igraph, jsonlite, kableExtra, knitr, latticeExtra, lwgeom, mapview, markdown, odbc, openxlsx, plotly, raster, RColorBrewer, Rcpp, RcppArmadillo, RCurl, readxl, rgdal, rmarkdown, roxygen2, RSQLite, sf, snow, sp, stars, tidyverse, xml2, Metrics, ROCR, spatialEco, randomForest, rfUtilities**
    -  Or you can run the following command in the Console within R Studio: **install.packages("tidyverse", "readxl", "lubridate", "plotly", "clipr", "curl", "data.table", "DBI", "dbplyr", "devtools", "doParallel", "FedData", "foreach", "future", "future.apply", "googlesheets4", "gridExtra", "here", "htmltools", "htmlwidgets", "httr", "igraph", "jsonlite", "kableExtra", "knitr", "latticeExtra", "lwgeom", "mapview", "markdown", "odbc", "openxlsx", "plotly", "raster", "RColorBrewer", "Rcpp", "RcppArmadillo", "RCurl", "readxl", "rgdal", "rmarkdown", "roxygen2", "RSQLite", "sf", "snow", "sp", "stars", "tidyverse", "xml2", "Metrics", "ROCR", "spatialEco", "randomForest", "rfUtilities")**


## Check out the Wiki!
I will be adding more information to the Wiki page in this repository. You have already visited one of the pages for the [Workshop Software Installation](https://gitlab.com/salbeke/introduction-to-r-and-spatial-r/-/wikis/Installing-Program-R-and-other-Software). The [Home page](https://gitlab.com/salbeke/introduction-to-r-and-spatial-r/-/wikis/home) has some additional resources, and I will probably add some video demos doing the same things we do in the workshop just for reference. You can also find the Wiki by finding the link on the right side of this GitLab repo (it says Wiki in the table of contents). To the right of the Home page, you will see other pages as they appear or are created.   
